import { api } from "./Provider";

export async function getItems() {
  try {
    const { data } = await api.get("/5d6fb6b1310000f89166087b");
    return data.vagas;
  } catch (error) {
    return error;
  }
}
