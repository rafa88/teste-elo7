import React from "react";

import HeaderComponent from "./Component/Hearder/HeaderComponent";
import WorkWithUs from "./Component/WorkWithUs/WorkWithUs";
import StyleElo7 from "./Component/StyleElo7/StyleElo7";
import CeoWord from "./Component/CeoWord/CeoWord";
import OurTeam from "./Component/OurTeam/OurTeam";

import "./Styles/App.scss";

const App = () => {
  const myDivToFocus = React.useRef();

  const handleOnClick = () => {
    myDivToFocus.current.scrollIntoView({ behavior: "smooth", block: "start" });
  };

  return (
    <div className="main">
      <HeaderComponent handleOnClick={handleOnClick} />

      <div className="gray padding-default">
        <CeoWord />

        <OurTeam />
      </div>

      <StyleElo7 handleOnClick={handleOnClick} />

      <div ref={myDivToFocus}>
        <WorkWithUs />
      </div>
    </div>
  );
};

export default App;
