import React from 'react';
import StyleElo7Atividade from '../../images/atividades.png';
import StyleElo7Descrontracao from '../../images/descontracao.png';
import StyleElo7Qualidade from '../../images/qualidade.png';

import './StyleElo7.scss';

const StyleElo7 = ({ handleOnClick }) => {
  return (
    <section className="style-elo7 padding-default">
      <div className="style-elo7-list flex">
        <div className="style-elo7-item">
          <img src={StyleElo7Qualidade} className="App-logo" alt="Foto do header" />
          <p className="subtitle uppercase">Qualidade de vida</p>
          <p className="style-elo7-item-description">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </p>
        </div>
        <div className="style-elo7-item">
          <img src={StyleElo7Descrontracao} className="App-logo" alt="Foto do header" />
          <p className="subtitle uppercase">Ambiente descontraído</p>
          <p className="style-elo7-item-description">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </p>
        </div>
        <div className="style-elo7-item">
          <img src={StyleElo7Atividade} className="App-logo" alt="Foto do header" />
          <p className="subtitle uppercase">Atividades extras</p>
          <p className="style-elo7-item-description">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </p>
        </div>
      </div>
      <button className="link uppercase text-center" onClick={handleOnClick}>Saiba mais</button>
    </section>
  )
};

export default StyleElo7;
