import React from 'react';
import './HeaderComponent.scss';

const HeaderComponent = ({ handleOnClick }) => {
  return (
    <header className="header text-center">
        <div className="banner">
          <h1>Trabalhe na Elo7</h1>
        </div>
        <p className="text-center">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
      </p>
      <button className="link uppercase text-center" onClick={handleOnClick}>Vagas em Aberto</button>
    </header>
  )
}

export default HeaderComponent;
