import React, { useEffect, useState } from "react";

import placeholder from "../../images/foto-bottom.png";
import Job from "../Job/Job";

import "./WorkWithUs.scss";
import { getItems } from "../../Services/JobsServices";

const WorkWithUs = () => {
  const [jobs, setJobs] = useState([]);

  useEffect(() => {
    const getJobs = async () => {
      const jobs = await getItems();
      setJobs(jobs);
    };
    getJobs();
  }, [setJobs]);

  return (
    <div className="jobs padding-default">
      <div className="jobs-photo">
        <img src={placeholder} alt="Foto do clima da Elo7" />
      </div>

      <h3 className="subtitle uppercase text-center">Vagas em Aberto</h3>
      <h4 className="subtitle uppercase">Desenvolvimento</h4>

      {jobs.map((job, index) => (
        <Job job={job} key={`job_${index}`} />
      ))}
    </div>
  );
};

export default WorkWithUs;
