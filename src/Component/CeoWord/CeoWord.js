import React from 'react';
import placeholderVideo from '../../images/placeholder-video.png';

import './CeoWord.scss';

const CeoWord = () => {
  return (
    <section className="ceo-word flex">
      <div className="ceo-word-video">
        <img src={placeholderVideo} alt="Video sobre a palavra do CEO" />
      </div>
      <div className="ceo-word-description">
        <h3 className="subtitle uppercase">Palavra do CEO<br /><small>Carlos Curioni</small></h3>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </p>
      </div>
    </section>
  );
}

export default CeoWord;
