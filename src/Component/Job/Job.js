import React from "react";

import "./Job.scss";

const Job = ({ job }) => {
  return (
    <div className="job flex">
      <p className="job-title">{job.cargo}</p>
      <p className="job-address">{job.localizacao ? `${job.localizacao.bairro} - ${job.localizacao.cidade}, ${job.localizacao.pais}` : 'Remota'}</p>
    </div>
  )
}

export default Job;