import React from 'react';
import team1 from '../../images/camila.png';
import team2 from '../../images/guto.png';
import team3 from '../../images/david.png';
import team4 from '../../images/beatriz.png';

import './OurTeam.scss';

const OurTeam = () => {
  return (
    <section className="our-team">
      <h3 className="subtitle uppercase text-center">Conheça nosso time<br /> fora de sério</h3>
      <ul>
        <li><img src={team1} alt="Video sobre a palavra do CEO" /></li>
        <li><img src={team2} alt="Video sobre a palavra do CEO" /></li>
        <li><img src={team3} alt="Video sobre a palavra do CEO" /></li>
        <li><img src={team4} alt="Video sobre a palavra do CEO" /></li>
      </ul>
    </section>
  );
}

export default OurTeam;
